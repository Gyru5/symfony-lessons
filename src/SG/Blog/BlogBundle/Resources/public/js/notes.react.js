/**
 * Created by gyrus on 25.01.17.
 */
var CommentSection = React.createClass({
    getInitialState: function() {
        return {
            comments: []
        }
    },
    componentDidMount: function() {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, 2000);
    },
    loadCommentsFromServer: function() {
        $.ajax({
            url: this.props.url,
            success: function (data) {
                this.setState({comments: data.comments});
            }.bind(this)
        });
    },
    render: function() {
        return (
            <div>
            <div className="comments-container">
            <h2 className="comments-header">Comments</h2>
            </div>
            <CommentList comments={this.state.comments} />
        </div>
        );
    }
});
var CommentList = React.createClass({
    render: function() {
        var commentNodes = this.props.comments.map(function(comment) {
            return (
                <CommentBox author={comment.author} date={comment.date} key={comment.id}>{comment.comment}</CommentBox>
            );
        });
        return (
            <section id="cd-timeline">
            {commentNodes}
            </section>
        );
    }
});
var CommentBox = React.createClass({
    render: function() {
        return (
            <div className="cd-timeline-block">
            <div className="cd-timeline-content">
            <h2><a href="#">{this.props.author}</a></h2>
        <p>{this.props.children}</p>
        <span className="cd-date">{this.props.date}</span>
        </div>
        </div>
        );
    }
});
window.CommentSection = CommentSection;