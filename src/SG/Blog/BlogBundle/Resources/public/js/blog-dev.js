/**
 * Created by gyrus on 25.01.17.
 */
jQuery(document).ready(function () {
    console.log('test');
    jQuery('[name=comment_add_from_post_page]').on('submit', function (event) {
        event.preventDefault();
        var $action = jQuery(this).attr('action');
        var $method = jQuery(this).attr('method');
        var $data = jQuery(this).serializeArray();
        jQuery.ajax({
            url: $action,
            data: $data,
            method: $method,
            success: function (data) {
                console.log(data);
            }
        })
    })
});