<?php

namespace SG\Blog\BlogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SG\Blog\ModelBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class IndexController extends Controller
{
    /**
     * @Route("{_locale}", requirements={"_locale"="en|ru"}, defaults={"_locale"="ru"}, name="index_action")
     */
    public function indexAction()
    {
        $posts = $this->getDoctrine()->getRepository('SGBlogModelBundle:Post')->findLast(10);

        if (null === $posts) {
            throw $this->createNotFoundException('There is now posts');
        }

        return $this->render(
            'SGBlogBlogBundle:Index:index.html.twig',
            [
                'posts' => $posts,
            ]
        );
    }

    /**
     * @Route(
     *     "{_locale}/auth/login",
     *     requirements={"_locale"="en|ru"},
     *     defaults={"_locale"="ru"},
     *     name="login_without_entity")
     */
    public function authLoginAction(Request $request)
    {
        $form = $this->createFormBuilder(null)
            ->add('name', TextType::class)
            ->add('password', PasswordType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {

        }

        return $this->render(
            'SGBlogBlogBundle:Auth:login.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Show a single post
     *
     * @param string $slug
     *
     * @Route("{_locale}/{slug}",
     *     requirements={"_locale"="en|ru"},
     *     defaults={"_locale"="ru"})
     *
     * @return Response
     * @Template()
     * @throws NotFoundHttpException
     */
    public function showAction($slug)
    {
        $post = $this->getDoctrine()->getRepository('SGBlogModelBundle:Post')->findBy(['slug' => $slug]);
//        dump($post);
        if (null === $post) {
            throw $this->createNotFoundException('Post was not found');
        }

        return $this->render(
            '@SGBlogBlog/Index/show.html.twig',
            [
                'post' => $post,
            ]
        );
    }

}
