<?php

namespace SG\Blog\ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SG\Comment\CoreBundle\Entity\Comment;

/**
 * Author
 *
 * @ORM\Table(name="author")
 * @ORM\Entity(repositoryClass="SG\Blog\ModelBundle\Repository\AuthorRepository")
 */
class Author extends TimeStamble
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="SG\Comment\CoreBundle\Entity\Comment", inversedBy="author", cascade={"remove"})
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id")
     */
    private $comment;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param \SG\Comment\CoreBundle\Entity\Comment $comment
     *
     * @return Author
     */
    public function setComment(Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \SG\Blog\ModelBundle\Entity\Comment
     */
    public function getComment()
    {
        return $this->comment;
    }
}
