<?php

namespace SG\Blog\ModelBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="SG\Blog\ModelBundle\Entity\Post", mappedBy="user")
     */
    private $post;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Add post
     *
     * @param \SG\Blog\ModelBundle\Entity\Post $post
     * @return User
     */
    public function addPost(Post $post)
    {
        $this->post[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \SG\Blog\ModelBundle\Entity\Post $post
     */
    public function removePost(Post $post)
    {
        $this->post->removeElement($post);
    }

    /**
     * Get post
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPost()
    {
        return $this->post;
    }
}
