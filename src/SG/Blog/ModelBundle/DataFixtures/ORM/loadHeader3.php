<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 03.03.17
 * Time: 17:27
 */

namespace SG\Blog\ModelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use SG\Blog\ModelBundle\Entity\Header;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class loadHeader3 implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
//        $header = new Header();
//        $header->setSlogan('Site Header Title');
//        $filePath = __DIR__ . "/../../Resources/public/image/logo.jpg";
//        $file = new UploadedFile($filePath, 'logo.jpg');
//        $uploader = $this->container->get('sg_blog_core.services.file_uploader');
//        $filename = $uploader->upload($file);
//        $header->setLogo($filename);
//
//        $manager->persist($header);
//        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 3;
    }
}