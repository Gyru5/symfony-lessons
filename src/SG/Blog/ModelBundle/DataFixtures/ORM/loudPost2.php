<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 25.12.16
 * Time: 1:36
 */

namespace SG\Blog\ModelBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use SG\Blog\ModelBundle\Entity\Post;

class loudPost2 implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.
        $post = new Post();
        $post->setTitle('lorem ipsum');
        $post->setBody('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.');
        $post->setUser($this->getUser($manager, 'admin'));

        $manager->persist($post);
        $manager->flush();
    }

    private function getUser(ObjectManager $manager, $name)
    {
        return $manager->getRepository('SGBlogModelBundle:User')->findOneBy(['username'   =>  $name]);
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}