<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 25.12.16
 * Time: 0:26
 */

namespace SG\Blog\ModelBundle\fixtur;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use SG\Blog\ModelBundle\Entity\User;

class loudUser1 implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('i.timchur@secl.com.ua');
        $userAdmin->setRoles(array('ROLE_ADMIN'));
        $userAdmin->setPlainPassword('test');
        $userAdmin->setEnabled(1);

        $manager->persist($userAdmin);
        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}