<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 30.01.17
 * Time: 20:03
 */

namespace SG\Blog\CoreBundle\Services;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthorManager
{
    private $em;

    /**
     * AuthorManager constructor.
     *
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * find author by name
     *
     * @param string $name
     *
     * @throws NotFoundHttpException
     * @return object|\SG\Blog\ModelBundle\Entity\Author
     */
    public function findByName($name)
    {
        $author = $this->em->getRepository('SGBlogModelBundle:Author')->findOneBy(['name' => $name]);

        if(null === $author){
            throw new \Exception('Author was not found');
        }

        return $author;
    }
}