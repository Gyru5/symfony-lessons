<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 28.12.16
 * Time: 13:47
 */

namespace SG\Blog\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;

class Builder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createSiteMenu(array $options)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav navbar-nav'));

        $menu->addChild('Home', array('route' => 'index_action'));
        // ... add more children

//        $menu->addChild('Post', ['route' => 'admin_post_index']);

        return $menu;
    }

    public function createAdminMenu(array $options)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav navbar-nav'));

        $menu->addChild('Admin', array('route' => 'sg_blog_admin_default_index'));
        // ... add more children

        $menu->addChild('Post', ['route' => 'admin_post_index']);
        $menu->addChild('Header', ['route'  =>  'header_index']);

        return $menu;
    }
}