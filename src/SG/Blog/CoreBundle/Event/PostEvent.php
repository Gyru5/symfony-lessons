<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 05.01.17
 * Time: 16:10
 */

namespace SG\Blog\CoreBundle\Event;


use SG\Blog\ModelBundle\Entity\Post;
use Symfony\Component\EventDispatcher\Event;

class PostEvent extends Event
{
    /**
     * @var \SG\Blog\ModelBundle\Entity\Post
     */
    private $post;

    /**
     * PostEvent constructor.
     *
     * @param \SG\Blog\ModelBundle\Entity\Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @return \SG\Blog\ModelBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }
}