<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 05.01.17
 * Time: 11:25
 */

namespace SG\Blog\CoreBundle\Listener;


use SG\Blog\CoreBundle\Event\PostEvent;
use SG\Blog\CoreBundle\SGBlogCoreEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class FlashListener implements EventSubscriberInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;

    /**
     * FlashListener constructor.
     * @param $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            SGBlogCoreEvents::POST_CREATED => 'onFlash',
            SGBlogCoreEvents::POST_UPDATED => 'onFlash'
        ];
    }

    /**
     * @param \SG\Blog\CoreBundle\Event\PostEvent $event
     */
    public function onFlash(PostEvent $event)
    {
        $post = $event->getPost();
        $this->session->getFlashBag()->add(
            'success',
            sprintf(
                'Запись %s отправлена на модерацию',
                $post->getTitle()
            ));
    }
}