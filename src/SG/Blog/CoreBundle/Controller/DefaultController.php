<?php

namespace SG\Blog\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SGBlogCoreBundle:Default:index.html.twig');
    }
}
