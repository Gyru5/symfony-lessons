<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 28.02.17
 * Time: 3:01
 */

namespace SG\Blog\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HeaderController extends Controller
{
    /**
     * @Route("{_locale}",
     *     requirements={"_locale"="en|ru"},
     *     defaults={"_locale"="ru"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function headerAction()
    {
        return $this->render("@SGBlogCore/Common/Header.html.twig");
    }
}