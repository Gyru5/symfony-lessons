<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 05.01.17
 * Time: 16:06
 */

namespace SG\Blog\CoreBundle;


final class SGBlogCoreEvents
{
    const POST_CREATED = 'post.created';
    const POST_UPDATED = 'post.updated';
}