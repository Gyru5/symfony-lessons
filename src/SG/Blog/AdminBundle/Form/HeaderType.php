<?php

namespace SG\Blog\AdminBundle\Form;

use SG\Blog\CoreBundle\Form\ImageType;
use SG\Blog\ModelBundle\Entity\Header;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HeaderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slogan', TextType::class)
//            ->add('image', EntityType::class, array(
//                'class' =>  'SGBlogModelBundle:Image'
//            ))
            ->add('image', ImageType::class)
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Header::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sg_blog_modelbundle_header';
    }


}
