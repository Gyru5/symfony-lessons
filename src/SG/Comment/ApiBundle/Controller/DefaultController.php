<?php

namespace SG\Comment\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use SG\Comment\CoreBundle\Entity\Comment;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View as FOSView;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends FOSRestController
{
    /**
     * @param $data
     *
     * @return \FOS\RestBundle\View\View
     */
    private function setSerializerGroup($data)
    {
        $view = $this->view($data, 200);
        $view->setContext($view->getContext()->setGroups(['all_user']));

        return $view;
    }

    /**
     * Return comment by id
     *
     * @param Comment $data Comment_id
     *
     * @return FOSView
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Shows comment by id",
     *  views = { "default", "premium" }
     * )
     */
    public function getCommetAction(Comment $data)
    {
        $view = $this->setSerializerGroup($data);

        return $this->handleView($view);
    }

    /**
     * Return comment by post
     *
     * @param integer $post Post id
     *
     * @return FOSView
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Shows comment by post",
     *  views = { "default", "premium" }
     * )
     */
    public function getCommentPostAction($post)
    {
        $data = $this->getDoctrine()->getRepository('SGCommentCoreBundle:Comment')->findBy(['post' => $post]);

        $view = $this->setSerializerGroup($data);

        return $this->handleView($view);
    }

    /**
     * Return counted comment by post
     *
     * @param integer $post Post id
     *
     * @return FOSView
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Shows counted comments by post",
     *  views = { "default", "premium" }
     * )
     */
    public function getCommentCountPostAction($post)
    {
        $data = $this->getDoctrine()->getRepository('SGCommentCoreBundle:Comment')->countComments($post);
        $data = ['count' => $data];
        $view = $this->setSerializerGroup($data);

        return $this->handleView($view);
    }


    /**
     * Return a comment for post
     *
     * @param Request $request
     *
     * @return FOSView
     *
     * @ApiDoc(
     *  resource=true,
     *  description="This method will create a comment for a post",
     *     input="SG\Comment\ApiBundle\Form\CommentType",
     *     output={
     *      "class" = "SG\Comment\CoreBundle\Entity\Comment",
     *      "groups" = {"all_user"},
     *      "parser"={"Nelmio\ApiDocBundle\Parser\JmsMetadataParser"}
     *     },
     *     statusCodes={
     *          "201"="Comment successfully created",
     *          "400"="Request was not correct"
     *     }
     * )
     *
     */
    public function postCommentAction(Request $request)
    {
        $comment = new Comment();
        $formType = $this->get('sg_comment_api.form.comment_type');
        $form = $this->createForm($formType, $comment);
        $isMethod = $request->isMethod('POST');
        $isValid = $form->handleRequest($request)->isValid();

        if ($isMethod && $isValid) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $view = $this->setSerializerGroup($comment);
            $view->setStatusCode(201);

            return $this->handleView($view);
        }

        return $this->handleView($this->view($form));
    }

    /**
     * Delete comment
     *
     * @param Comment $id
     *
     * @return FOSView
     *
     * @ApiDoc(
     *  resource=true,
     *  description="This method will delete a comment",
     *     statusCodes={
     *          "200"="Comment successfully created",
     *          "400"="Request was not correct",
     *          "404"="Not found"
     *     }
     * )
     *
     */
    public function deleteCommentAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('SGCommentCoreBundle:Comment')->find($id);
        if ($comment instanceof Comment) {
            $em->remove($comment);
            $em->flush();
            $view = $this->view(['message' => 'Comment deleted']);
        } else {
            $view = $this->view(['message' => 'Comment was not found']);
        }
        /** FOSView $view */
        return $this->handleView($view);
    }
}
