<?php

namespace SG\Comment\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testCommentsGet()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/v1/comments.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'The response was successful');
    }

    public function testCommentsForPostGet()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/comments/1/post.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'The response was successful');
    }

    public function testCommentsByIdGet()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/comments/1.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'The response was successful');
    }


    public function testPostComment()
    {
        $client = static::createClient();

        $post = $client->getContainer()->get('doctrine')->getRepository('SGBlogModelBundle:Post')->findFirstPost();

        $param = [
            'sg_comment_corebundle_comment' => [
                '[comment]' => 'test comment',
                '[post]' => $post->getId(),
            ],
        ];

        $client->request('POST', '/api/v1/comments.json', $param);
        $this->assertEquals(201, $client->getResponse()->getStatusCode(), 'Creation was successful');
    }

    public function testDeleteComment()
    {
        $client = static::createClient();

        $id = $client->getContainer()->get('doctrine')->getRepository('SGCommentCoreBundle:Comment')->findBy(null, ['id' => 'DESC'], 1)->getId();

        $client->request('DELETE', '/api/v1/comments/'.$id.'.json');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Comment deleted');
    }
}
