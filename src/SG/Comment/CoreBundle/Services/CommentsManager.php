<?php

namespace SG\Comment\CoreBundle\Services;


use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use SG\Comment\CoreBundle\Entity\Comment;
use SG\Blog\ModelBundle\Entity\Post;
use SG\Comment\CoreBundle\SGCommentCoreBundle;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentsManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /**
     * @var \Symfony\Component\Form\FormFactory
     */
    private $formFactory;
    /**
     * @var \Symfony\Component\Form\FormFactoryInterface
     */
    private $form;
    /**
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $securityContext;
    /**
     * @var \Symfony\Component\Security\Acl\Model\MutableAclProviderInterface
     */
    private $aclProvider;
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     * CommentsManager constructor.
     *
     * @param \Doctrine\ORM\EntityManager                                       $em
     * @param \Symfony\Component\Form\FormFactory                               $formFactory
     * @param \Symfony\Component\Form\FormFactoryInterface                      $formFactoryInterface
     * @param \Symfony\Component\Security\Core\SecurityContextInterface         $securityContext
     * @param \Symfony\Component\Security\Acl\Model\MutableAclProviderInterface $mutableAclProvider
     */
    public function __construct(
        EntityManager $em,
        FormFactory $formFactory,
        FormFactoryInterface $formFactoryInterface,
        SecurityContextInterface $securityContext,
        MutableAclProviderInterface $mutableAclProvider,
        Container $container
)
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->form = $formFactoryInterface;
        $this->securityContext = $securityContext;
        $this->aclProvider = $mutableAclProvider;
        $this->container = $container;
    }

    /**
     * find all post comments
     *
     * @param \SG\Blog\ModelBundle\Entity\Post $post
     *
     * @return array|\SG\Comment\CoreBundle\Entity\Comment
     */
    public function getPostComments(Post $post)
    {
        $comments = $this->em->getRepository('SGBlogModelBundle:Comment')->findBy(['post' => $post]);

        return $comments;
    }

    /**
     * @param \SG\Blog\ModelBundle\Entity\Post          $post
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return bool|\Symfony\Component\Form\FormView
     */
    public function createComments(Post $post, Request $request)
    {
        $comment = new Comment();
        $comment->setPost($post);

        $form = $this->formFactory->create($this->form, $comment);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $this->em->persist($comment);
            $this->em->flush();

            $user = $this->securityContext->getToken()->getUser();
            if($user instanceof UserInterface)
            {
                //Create ACL
                $objectIdentity = ObjectIdentity::fromDomainObject($comment);
                $acl = $this->aclProvider->createACL($objectIdentity);
                //create user Identity
                $securityIdentity = UserSecurityIdentity::fromAccount($user);
                //create user rights
                $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
                $this->aclProvider->updateAcl($acl);
            }

            return true;
        }

        return $form->createView();
    }

    /**
     * @param \SG\Comment\CoreBundle\Entity\Comment $comment
     * @param                                       $id
     *
     * @return \Symfony\Component\Form\FormView
     * @throws \Exception
     */
    public function updateComment(Request $request, $id){

        $comment = $this->em->getRepository('SGCommentCoreBundle:Comment')->findBy(['id' => $id]);

        if(!$comment) {
            throw new \Exception('Unable to find comment');
        }
        $securityContext = $this->container->get('security.context');
        if(false === $securityContext->isGranted('EDIT', $comment)){
            throw  new AccessDeniedHttpException('Access denied');
        }

        $form = $this->formFactory->createBuilder('SG\Comment\CoreBundle\Form\CommentType', $comment, [
        ])
        ->getForm();

        $form->handleRequest($request);

        if($form->isValid())
        {

        }

        return $form->createView();

    }
}