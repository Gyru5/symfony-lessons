<?php

namespace SG\Comment\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class SGCommentCoreExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
//    public function load(array $configs, ContainerBuilder $container)
//    {
//        $configuration = new Configuration();
//        $config = $this->processConfiguration($configuration, $configs);
//
//        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
//        $loader->load('services.yml');
//    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $claaName = $config['entity']['class'];
        $container->setParameter('comment_core.subject_entity.class', $claaName);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * @inheritDoc
     */
    public function prepend(ContainerBuilder $container)
    {
        // get all bundles
        $bundles = $container->getParameter('kernel.bundles');

        if (isset($bundles['DoctrineBundle'])) {
            $configuration = new Configuration();
            $configs = $container->getExtensionConfig($this->getAlias());
            $config = $this->processConfiguration($configuration, $configs);

            //prepare array to insert
            $forInsert = [
                'orm' => [
                    'resolve_target_entities' =>
                        [
                            'SG\Comment\CoreBundle\Model\CommentSubjectInterface'
                            => $config['entity']['class'],
                        ],
                ],
            ];

            //insert config to doctrine configuration
            $container->prependExtensionConfig('doctrine', $forInsert);
        }
    }
}
