<?php

namespace SG\Comment\CoreBundle\Model;

/**
 * Interface CommentSubjectInterface
 *
 */
interface CommentSubjectInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @return string
     */
    public function getSlug();
}