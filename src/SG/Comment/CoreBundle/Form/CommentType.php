<?php

namespace SG\Comment\CoreBundle\Form;

use SG\Blog\ModelBundle\Entity\Author;
use SG\Blog\BlogBundle\Form\AuthorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', CollectionType::class, [
                'entry_type' => AuthorType::class,
//                'class'    =>  "SGBlogModelBundle:Author",
                'label' => 'Введите Ваше имя',
//                'empty_data' => null
            ])
//            ->add('author', AuthorType::class)
            ->add('comment', TextareaType::class, [
                'label' => 'Введите Ваш комментарий'
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SG\Comment\CoreBundle\Entity\Comment'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sg_comment_corebundle_comment';
    }


}
