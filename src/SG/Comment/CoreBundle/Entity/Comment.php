<?php

namespace SG\Comment\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use SG\Blog\ModelBundle\Entity\Author;
use SG\Blog\ModelBundle\Entity\TimeStamble;
use SG\Comment\CoreBundle\Model\CommentSubjectInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Comment
 *
 * @Serializer\ExclusionPolicy("all")
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="SG\Comment\CoreBundle\Repository\CommentRepository")
 */
class Comment extends TimeStamble
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"all_user"})
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Expose()
     * @Serializer\Groups({"all_user"})
     *
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @ORM\OneToOne(targetEntity="SG\Blog\ModelBundle\Entity\Author", mappedBy="comment", cascade={"remove"})
     */
    private $author;

    private $post;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set author
     *
     * @param \SG\Blog\ModelBundle\Entity\Author $author
     *
     * @return Comment
     */
    public function setAuthor(Author $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \SG\Blog\ModelBundle\Entity\Author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set post
     *
     * @param \SG\Comment\CoreBundle\Model\CommentSubjectInterface $post
     *
     * @return Comment
     */
    public function setPost(CommentSubjectInterface $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \SG\Comment\CoreBundle\Model\CommentSubjectInterface
     */
    public function getPost()
    {
        return $this->post;
    }
}
