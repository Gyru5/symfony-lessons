<?php

namespace SG\Comment\CoreBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Events;

//use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class DynamicRelationSubscriber implements EventSubscriber
{
    const COMMENT_ENTITY_PATH = 'SG\Comment\CoreBundle\Entity\Comment';

    /**
     * @var string
     */
    private $resolve_class;

    public function __construct($resolveClass)
    {
        $this->resolve_class = $resolveClass;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [Events::loadClassMetadata];
    }

    /**
     * @param LoadClassMetadataEventArgs $args
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        // Get class metadata
        /** @var ClassMetadata $metadata */
        $metadata = $args->getClassMetadata();

        // Reject other classes
        if ($metadata->getName() != self::COMMENT_ENTITY_PATH) {
            return;
        }

        // Get  naming strategy
        $namingStrategy = $args
            ->getEntityManager()
            ->getConfiguration()
            ->getNamingStrategy();

        // Config the mapping
        $metadata->mapManyToOne([
            'targetEntity' => $this->resolve_class,
            'fieldName' => 'post',
            'joinTable' => [
                'name' => strtolower($namingStrategy->classToTableName($metadata->getName())),
                'joinColumns' => [
                    'name' => $namingStrategy->joinKeyColumnName($metadata->getName()),
                    'referenceColumnName' => $namingStrategy->referenceColumnName(),
                    'nulable' => 'false',
                    'onDelete' => 'CASCADE'
                ]
            ]
        ]);
    }
}