<?php
/**
 * Created by PhpStorm.
 * User: gyrus
 * Date: 25.01.17
 * Time: 18:56
 */

namespace SG\Comment\CoreBundle\Controller;


use SG\Blog\BlogBundle\Form\CommentType;
use SG\Comment\CoreBundle\Entity\Comment;
use SG\Blog\ModelBundle\Entity\Post;
use SG\Comment\CoreBundle\Model\CommentSubjectInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CommentController extends Controller
{
    /**
     * @Route("{slug}/comments", name="show_comments")
     * @Method("GET")
     */
    public function showCommentsAction()
    {
        $comments = [
            ['id' => 1, 'author' => 'Ivan', 'note' => "First comment", 'date' => 'Jan. 25, 2017'],
            ['id' => 2, 'author' => 'Henry', 'note' => "Second comment", 'date' => 'Jan. 25, 2017'],
            ['id' => 3, 'author' => 'Lev', 'note' => "Third comment", 'date' => 'Jan. 25, 2017'],
        ];

        $data = [
            'comments' => $comments,
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/comment/add", name="add_comment")
     */
    public function addCommentAction(Post $post, Request $request)
    {
        $comManager = $this->get('sg_comment_core.service.comment_manager');

        if (true === $comManager->createComments($post, $request)) {
            return new JsonResponse('done');
        }

        return $this->render(
            'SGCommentCoreBundle:Comment:new.html.twig',
            array(
                'form' => $comManager->createComments($post, $request),
            )
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param                                           $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateCommentAction(Request $request, $id)
    {
        $editForm = $this->get('sg_comment_core.service.comment_manager');
        $editForm->updateComment($request, id);

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createUpdateForm($entity);

        return new Response();
    }

    /**
     * Deletes a comment entity.
     *
     * @Route("/{id}", name="comment_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Comment $comment)
    {
        $form = $this->createDeleteForm($comment);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if($request->isXmlHttpRequest()){

            if ($form->isValid()){
                $em->remove($comment);
                $em->flush();

                return new JsonResponse([
                    'result' => 'success'
                ]);
            }
        } else {
            if ($form->isSubmitted() && $form->isValid()) {

                $em->remove($comment);
                $em->flush();
            }
            return $this->redirectToRoute('post_index');
        }

        return false;
    }

    /**
     * Creates a form to delete a post entity.
     *
     * @param Post $post The post entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comment $comment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comment_delete', array('id' => $comment->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    /**
     * @Route("/count_comments_number/{post}", name="countComments")
     * @param \SG\Comment\CoreBundle\Model\CommentSubjectInterface $post
     *
     * @return
     */
    public function countCommentNumbersAction(CommentSubjectInterface $post)
    {
        $count = $this->getDoctrine()->getManager()->getRepository('SG\Comment\CoreBundle\Entity\Comment')->countComments($post);

        return $this->render(
            'SGCommentCoreBundle:Comment:count.html.twig',
            array(
                'count' => $count
            )
        );
    }
}