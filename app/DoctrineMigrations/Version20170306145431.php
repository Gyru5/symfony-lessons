<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170306145431 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE header ADD image_id INT DEFAULT NULL, DROP image_name');
        $this->addSql('ALTER TABLE header ADD CONSTRAINT FK_6E72A8C13DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6E72A8C13DA5256D ON header (image_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE header DROP FOREIGN KEY FK_6E72A8C13DA5256D');
        $this->addSql('DROP INDEX UNIQ_6E72A8C13DA5256D ON header');
        $this->addSql('ALTER TABLE header ADD image_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP image_id');
    }
}
