<?php

namespace SG\Blog\BlogBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

//        $this->assertContains('Hello World', $client->getResponse()->getContent());
    }

    public function testShow()
    {
        $client = static::createClient();

        $post = $client->getContainer()->get('doctrine')->getRepository('SGBlogModelBundle:Post')->findFirstPost();

        $crawler = $client->request('GET', '/' . $post->getSlug());

        $this->assertTrue($client->getResponse()->isSuccessful(), 'The response was not successful');

        $this->assertEquals($post->getTitle(), $crawler->filter('h1')->text(), 'Post title is invalid');
    }
}
